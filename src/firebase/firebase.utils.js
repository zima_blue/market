import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCqRBwGEaPxSw0jfiDmX0HvQKod0AXP084",
  authDomain: "market-3cd2d.firebaseapp.com",
  databaseURL: "https://market-3cd2d.firebaseio.com",
  projectId: "market-3cd2d",
  storageBucket: "market-3cd2d.appspot.com",
  messagingSenderId: "196836702700",
  appId: "1:196836702700:web:24b6dc732dcd71170ae8c9",
  measurementId: "G-DJ2BD4T3B0"
}

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) {
    return;
  }

  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if(!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log('error creating user ', error.message);
    }
  }

  return userRef;
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
// firebase.firestore.setLogLevel('debug');

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;